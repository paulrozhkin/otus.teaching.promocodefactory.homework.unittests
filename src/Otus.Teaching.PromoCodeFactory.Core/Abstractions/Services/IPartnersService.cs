﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.Exceptions;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Services
{
    public interface IPartnersService
    {
        /// <summary>
        /// Add new limit partner limit.
        /// </summary>
        /// <exception cref="EntityNotFoundException">If partner not found by partnerId.</exception>
        /// <exception cref="PartnerNotActiveException">If partner is not active.</exception>
        /// <param name="partnerId">Partner partnerId.</param>
        /// <param name="newLimitInfo">Information about limit.</param>
        /// <returns></returns>
        Task<PartnerPromoCodeLimit> SetNewPromoCodeLimitAsync(Guid partnerId, PartnerPromoCodeLimit newLimitInfo);

        /// <summary>
        /// Cancel the first active partner limit.
        /// </summary>
        /// <exception cref="EntityNotFoundException">If partner not found by partnerId.</exception>
        /// <exception cref="PartnerNotActiveException">If partner is not active.</exception>
        /// <param name="partnerId">Partner partnerId.</param>
        /// <returns></returns>
        Task CancelActivePromoCodeLimitAsync(Guid partnerId);

        Task<IEnumerable<Partner>> GetAllAsync();

        Task<Partner> GetByIdAsync(Guid id);
    }
}
