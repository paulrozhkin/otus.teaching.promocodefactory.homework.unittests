﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Services;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.Exceptions;

namespace Otus.Teaching.PromoCodeFactory.Core.Services
{
    public class PartnersService : IPartnersService
    {
        private readonly IRepository<Partner> _partnersRepository;

        public PartnersService(IRepository<Partner> partnersRepository)
        {
            _partnersRepository = partnersRepository;
        }

        public async Task<PartnerPromoCodeLimit> SetNewPromoCodeLimitAsync(Guid partnerId,
            PartnerPromoCodeLimit newLimitInfo)
        {
            if (newLimitInfo.Limit <= 0)
                throw new InvalidLimitException("Limit must be greater than 0");

            var partner = await GetActivePartner(partnerId);

            //Установка лимита партнеру
            if (CancelActiveLimit(partner))
            {
                //Если партнеру выставляется лимит, то мы 
                //должны обнулить количество промокодов, которые партнер выдал, если лимит закончился, 
                //то количество не обнуляется
                partner.NumberIssuedPromoCodes = 0;
            }

            var newLimit = new PartnerPromoCodeLimit()
            {
                Limit = newLimitInfo.Limit,
                Partner = partner,
                PartnerId = partner.Id,
                CreateDate = DateTime.Now,
                EndDate = newLimitInfo.EndDate
            };

            partner.PartnerLimits.Add(newLimit);

            await _partnersRepository.UpdateAsync(partner);

            return newLimit;
        }

        public async Task CancelActivePromoCodeLimitAsync(Guid partnerId)
        {
            var partner = await GetActivePartner(partnerId);
            //Отключение лимита
            if (CancelActiveLimit(partner))
            {
                await _partnersRepository.UpdateAsync(partner);
            }
        }

        public Task<IEnumerable<Partner>> GetAllAsync()
        {
            return _partnersRepository.GetAllAsync();
        }

        public Task<Partner> GetByIdAsync(Guid id)
        {
            var partner = _partnersRepository.GetByIdAsync(id);

            if (partner == null)
                throw new EntityNotFoundException($"Partner {id} not found");

            return partner;
        }

        private async Task<Partner> GetActivePartner(Guid partnerId)
        {
            var partner = await _partnersRepository.GetByIdAsync(partnerId);

            if (partner == null)
                throw new EntityNotFoundException($"Partner {partnerId} not found");

            //Если партнер заблокирован, то нужно выдать исключение
            if (!partner.IsActive)
                throw new PartnerNotActiveException($"Partner {partnerId} is not active");

            return partner;
        }

        /// <summary>
        /// Cancel the active partner limit.
        /// </summary>
        /// <param name="partner">Mutable partner.</param>
        /// <returns>Active limit found and canceled.</returns>
        private bool CancelActiveLimit(Partner partner)
        {
            var activeLimit = partner.PartnerLimits.FirstOrDefault(x =>
                !x.CancelDate.HasValue);

            if (activeLimit != null)
            {
                activeLimit.CancelDate = DateTime.Now;
                return true;
            }

            return false;
        }
    }
}