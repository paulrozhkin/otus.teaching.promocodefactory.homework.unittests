﻿#nullable enable
using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Exceptions
{
    public class InvalidLimitException: Exception
    {
        public InvalidLimitException()
        {
        }

        public InvalidLimitException(string? message) : base(message)
        {
        }

        public InvalidLimitException(string? message, Exception? innerException) : base(message, innerException)
        {
        }
    }
}
