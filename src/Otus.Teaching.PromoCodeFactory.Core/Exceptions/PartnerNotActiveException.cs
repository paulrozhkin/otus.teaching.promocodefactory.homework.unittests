﻿#nullable enable
using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Exceptions
{
    public class PartnerNotActiveException: Exception
    {
        public PartnerNotActiveException()
        {
        }

        public PartnerNotActiveException(string? message) : base(message)
        {
        }

        public PartnerNotActiveException(string? message, Exception? innerException) : base(message, innerException)
        {
        }
    }
}
