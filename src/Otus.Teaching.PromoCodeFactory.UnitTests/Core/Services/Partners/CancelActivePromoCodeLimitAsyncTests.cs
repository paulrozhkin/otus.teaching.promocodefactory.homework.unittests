﻿using System;
using System.Collections.Generic;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.Exceptions;
using Otus.Teaching.PromoCodeFactory.Core.Services;
using Xunit;
using Shouldly;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Core.Services.Partners
{
    public class CancelActivePromoCodeLimitAsync
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersService _partnersService;
        private readonly IFixture _fixture;

        public CancelActivePromoCodeLimitAsync()
        {
            _fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = _fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersService = _fixture.Build<PartnersService>().OmitAutoProperties().Create();

            _fixture.Behaviors.Remove(new ThrowingRecursionBehavior());
            _fixture.Behaviors.Add(new OmitOnRecursionBehavior());
            _fixture.Customize<Partner>(c => c.With(p => p.IsActive, true));
        }

        [Fact]
        public async void CancelActivePromoCodeLimitAsync_PartnerIsNotFound_ThrowsEntityNotFoundException()
        {
            // Arrange
            var partnerId = _fixture.Create<Guid>();
            Partner partner = null;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act && Assert
            await _partnersService.CancelActivePromoCodeLimitAsync(partnerId)
                .ShouldThrowAsync<EntityNotFoundException>();
        }

        [Fact]
        public async void CancelActivePromoCodeLimitAsync_PartnerIsNotActive_ThrowsPartnerNotActiveException()
        {
            // Arrange
            var partner = _fixture.Build<Partner>().With(partner => partner.IsActive, false).Create();
            var partnerId = partner.Id;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act && Assert
            await _partnersService.CancelActivePromoCodeLimitAsync(partnerId)
                .ShouldThrowAsync<PartnerNotActiveException>();
        }

        [Fact]
        public async void CancelActivePromoCodeLimitAsync_PartnerNotHaveActiveLimit_SkipUpdatePartner()
        {
            // Arrange
            var partner = _fixture.Create<Partner>();
            var partnerId = partner.Id;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            await _partnersService.CancelActivePromoCodeLimitAsync(partnerId);

            // Assert
            _partnersRepositoryMock.Verify(x => x.UpdateAsync(It.IsAny<Partner>()), Times.Never);
        }

        [Fact]
        public async void CancelActivePromoCodeLimitAsync_PartnerHaveActiveLimit_PartnerUpdated()
        {
            // Arrange
            var activePromoCodeLimit = _fixture
                .Build<PartnerPromoCodeLimit>().Without(p => p.CancelDate).Create();

            var partner = _fixture
                .Build<Partner>()
                .With(p => p.PartnerLimits, new List<PartnerPromoCodeLimit> {activePromoCodeLimit}).Create();
            var partnerId = partner.Id;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            await _partnersService.CancelActivePromoCodeLimitAsync(partnerId);

            // Assert
            _partnersRepositoryMock.Verify(x => x.UpdateAsync(partner), Times.Once);
        }

        [Fact]
        public async void CancelActivePromoCodeLimitAsync_PartnerHaveActiveLimit_CancelLimit()
        {
            // Arrange
            var activePromoCodeLimit = _fixture
                .Build<PartnerPromoCodeLimit>().Without(p => p.CancelDate).Create();

            var partner = _fixture
                .Build<Partner>()
                .With(p => p.PartnerLimits, new List<PartnerPromoCodeLimit> {activePromoCodeLimit}).Create();
            var partnerId = partner.Id;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            await _partnersService.CancelActivePromoCodeLimitAsync(partnerId);

            // Assert
            activePromoCodeLimit.CancelDate.Should().NotBeNull();
        }
    }
}