﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.Exceptions;
using Otus.Teaching.PromoCodeFactory.Core.Services;
using Shouldly;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Core.Services.Partners
{
    public class SetNewPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly IFixture _fixture;
        private readonly PartnersService _partnersService;

        public SetNewPromoCodeLimitAsyncTests()
        {
            _fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = _fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersService = _fixture.Build<PartnersService>().OmitAutoProperties().Create();

            _fixture.Behaviors.Remove(new ThrowingRecursionBehavior());
            _fixture.Behaviors.Add(new OmitOnRecursionBehavior());

            _fixture.Customizations.Add(new RandomNumericSequenceGenerator(10, 30));
            _fixture.Customize<Partner>(c => c.With(p => p.IsActive, true));
        }

        [Fact]
        public async Task SetNewPromoCodeLimitAsync_PartnerIsNotFound_ThrowsEntityNotFoundException()
        {
            // Arrange
            var partnerId = _fixture.Create<Guid>();
            Partner partner = null;

            var limitInfo = _fixture.Create<PartnerPromoCodeLimit>();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act && Assert
            await _partnersService.SetNewPromoCodeLimitAsync(partnerId, limitInfo)
                .ShouldThrowAsync<EntityNotFoundException>();
        }

        [Fact]
        public async Task SetNewPromoCodeLimitAsync_PartnerIsNotActive_ThrowsBadRequestObjectResult()
        {
            // Arrange
            var partner = _fixture.Build<Partner>().With(p => p.IsActive, false).Create();

            var limitInfo = _fixture.Create<PartnerPromoCodeLimit>();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act && Assert
            await _partnersService.SetNewPromoCodeLimitAsync(partner.Id, limitInfo)
                .ShouldThrowAsync<PartnerNotActiveException>();
        }

        [Fact]
        public async Task SetNewPromoCodeLimitAsync_SetNewLimitWithAlreadyActiveLimit_ClearNumberIssuedPromoCodes()
        {
            // Arrange
            var activePromoCodeLimit = _fixture
                .Build<PartnerPromoCodeLimit>().Without(p => p.CancelDate).Create();

            var partner = _fixture
                .Build<Partner>()
                .With(p => p.PartnerLimits, new List<PartnerPromoCodeLimit> {activePromoCodeLimit}).Create();

            var limitInfo = _fixture.Create<PartnerPromoCodeLimit>();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            await _partnersService.SetNewPromoCodeLimitAsync(partner.Id, limitInfo);

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        [Fact]
        public async Task SetNewPromoCodeLimitAsync_SetNewLimitWithoutActiveLimit_NotChangeNumberIssuedPromoCodes()
        {
            // Arrange
            var partner = _fixture.Create<Partner>();
            var expectedNumberIssuedPromoCodes = partner.NumberIssuedPromoCodes;

            var limitInfo = _fixture.Create<PartnerPromoCodeLimit>();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            await _partnersService.SetNewPromoCodeLimitAsync(partner.Id, limitInfo);

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(expectedNumberIssuedPromoCodes);
        }

        [Fact]
        public async Task SetNewPromoCodeLimitAsync_SetNewLimitWithAlreadyActiveLimit_CancelActiveLimit()
        {
            // Arrange
            var activePromoCodeLimit = _fixture
                .Build<PartnerPromoCodeLimit>().Without(p => p.CancelDate).Create();

            var partner = _fixture
                .Build<Partner>()
                .With(p => p.PartnerLimits, new List<PartnerPromoCodeLimit> {activePromoCodeLimit}).Create();

            var limitInfo = _fixture.Create<PartnerPromoCodeLimit>();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            await _partnersService.SetNewPromoCodeLimitAsync(partner.Id, limitInfo);

            // Assert
            activePromoCodeLimit.CancelDate.Should().NotBeNull();
        }

        [Fact]
        public async Task SetNewPromoCodeLimitAsync_SetNegativeLimits_ThrowsInvalidLimitException()
        {
            // Arrange
            _fixture.Customizations.Insert(0, new RandomNumericSequenceGenerator(int.MinValue, -1));

            var partner = _fixture.Create<Partner>();
            var limitInfo = _fixture.Create<PartnerPromoCodeLimit>();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act && Assert
            await _partnersService.SetNewPromoCodeLimitAsync(partner.Id, limitInfo)
                .ShouldThrowAsync<InvalidLimitException>();
        }

        [Fact]
        public async Task SetNewPromoCodeLimitAsync_SetZeroLimits_ThrowsInvalidLimitException()
        {
            // Arrange
            var partner = _fixture.Create<Partner>();
            var limitInfo = _fixture.Build<PartnerPromoCodeLimit>().With(r => r.Limit, 0).Create();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act && Assert
            await _partnersService.SetNewPromoCodeLimitAsync(partner.Id, limitInfo)
                .ShouldThrowAsync<InvalidLimitException>();
        }

        [Fact]
        public async Task SetNewPromoCodeLimitAsync_SetPositiveLimits_SuccessfulCreatedNewLimit()
        {
            // Arrange
            _fixture.Customizations.Insert(0, new RandomNumericSequenceGenerator(1, int.MaxValue));

            var partner = _fixture.Create<Partner>();
            var limitInfo = _fixture.Create<PartnerPromoCodeLimit>();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersService.SetNewPromoCodeLimitAsync(partner.Id, limitInfo);

            // Assert
            result.Limit.Should().Be(limitInfo.Limit);
            result.EndDate.Should().Be(limitInfo.EndDate);
        }

        [Fact]
        public async Task SetNewPromoCodeLimitAsync_SetValidLimit_LimitAddedToRepository()
        {
            // Arrange
            var partner = _fixture.Create<Partner>();
            var limitInfo = _fixture.Create<PartnerPromoCodeLimit>();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            await _partnersService.SetNewPromoCodeLimitAsync(partner.Id, limitInfo);

            // Assert
            partner.PartnerLimits.Should().Contain(x => x.Limit == limitInfo.Limit && x.EndDate == limitInfo.EndDate);
            _partnersRepositoryMock.Verify(x => x.UpdateAsync(partner));
        }
    }
}