﻿using System;
using System.Threading.Tasks;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Services;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.Exceptions;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetNewPromoCodeLimitAsyncTests
    {
        private readonly Mock<IPartnersService> _partnersServiceMock;
        private readonly PartnersController _partnersController;
        private readonly IFixture _fixture;

        public SetNewPromoCodeLimitAsyncTests()
        {
            _fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersServiceMock = _fixture.Freeze<Mock<IPartnersService>>();
            _partnersController = _fixture.Build<PartnersController>().OmitAutoProperties().Create();

            _fixture.Behaviors.Remove(new ThrowingRecursionBehavior());
            _fixture.Behaviors.Add(new OmitOnRecursionBehavior());
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_ReceiveCorrectLimitInfo_ServiceWasInvokedAndReturnsCreated()
        {
            // Arrange
            var partnerId = _fixture.Create<Guid>();
            var request = _fixture.Create<SetPartnerPromoCodeLimitRequest>();
            var partnerPromoCode = _fixture.Build<PartnerPromoCodeLimit>()
                .With(promo => promo.EndDate, request.EndDate)
                .With(promo => promo.Limit, request.Limit)
                .Create();

            _partnersServiceMock.Setup(service => service.SetNewPromoCodeLimitAsync(partnerId,
                    It.Is<PartnerPromoCodeLimit>(x => x.EndDate == request.EndDate && x.Limit == request.Limit)))
                .Returns(Task.FromResult(partnerPromoCode));

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            _partnersServiceMock.Verify(service => service.SetNewPromoCodeLimitAsync(partnerId,
                    It.Is<PartnerPromoCodeLimit>(x => x.EndDate == request.EndDate && x.Limit == request.Limit)),
                Times.Once);
            result.Should().BeAssignableTo<CreatedAtActionResult>();
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            // Arrange
            var partnerId = _fixture.Create<Guid>();
            var request = _fixture.Create<SetPartnerPromoCodeLimitRequest>();

            _partnersServiceMock.Setup(service => service.SetNewPromoCodeLimitAsync(partnerId,
                    It.IsAny<PartnerPromoCodeLimit>()))
                .Throws<EntityNotFoundException>();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
            // Arrange
            var partnerId = _fixture.Create<Guid>();
            var request = _fixture.Create<SetPartnerPromoCodeLimitRequest>();

            _partnersServiceMock.Setup(service => service.SetNewPromoCodeLimitAsync(partnerId,
                    It.IsAny<PartnerPromoCodeLimit>()))
                .Throws<PartnerNotActiveException>();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_SetInvalidLimit_ReturnsBadRequests()
        {
            // Arrange
            var partnerId = _fixture.Create<Guid>();
            var request = _fixture.Create<SetPartnerPromoCodeLimitRequest>();

            _partnersServiceMock.Setup(service => service.SetNewPromoCodeLimitAsync(partnerId,
                    It.IsAny<PartnerPromoCodeLimit>()))
                .Throws<InvalidLimitException>();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }
    }
}