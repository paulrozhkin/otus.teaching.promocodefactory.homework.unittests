﻿using System;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Services;
using Otus.Teaching.PromoCodeFactory.Core.Exceptions;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class CancelActivePromoCodeLimitAsyncTests
    {
        private readonly Mock<IPartnersService> _partnersServiceMock;
        private readonly PartnersController _partnersController;
        private readonly IFixture _fixture;

        public CancelActivePromoCodeLimitAsyncTests()
        {
            _fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersServiceMock = _fixture.Freeze<Mock<IPartnersService>>();
            _partnersController = _fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        [Fact]
        public async void CancelPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            // Arrange
            var partnerId = _fixture.Create<Guid>();

            _partnersServiceMock.Setup(service => service.CancelActivePromoCodeLimitAsync(partnerId))
                .Throws<EntityNotFoundException>();

            // Act
            var result = await _partnersController.CancelPartnerPromoCodeLimitAsync(partnerId);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        public async void CancelPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
            // Arrange
            var partnerId = _fixture.Create<Guid>();

            _partnersServiceMock.Setup(service => service.CancelActivePromoCodeLimitAsync(partnerId))
                .Throws<PartnerNotActiveException>();

            // Act
            var result = await _partnersController.CancelPartnerPromoCodeLimitAsync(partnerId);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }
    }
}