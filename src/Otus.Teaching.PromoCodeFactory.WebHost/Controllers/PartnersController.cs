﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Services;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.Exceptions;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Партнеры
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PartnersController
        : ControllerBase
    {
        private readonly IPartnersService _partnersService;

        public PartnersController(IPartnersService partnersService)
        {
            _partnersService = partnersService;
        }

        [HttpGet]
        public async Task<ActionResult<List<PartnerResponse>>> GetPartnersAsync()
        {
            var partners = await _partnersService.GetAllAsync();

            var response = partners.Select(x => new PartnerResponse()
            {
                Id = x.Id,
                Name = x.Name,
                NumberIssuedPromoCodes = x.NumberIssuedPromoCodes,
                IsActive = true,
                PartnerLimits = x.PartnerLimits
                    .Select(y => new PartnerPromoCodeLimitResponse()
                    {
                        Id = y.Id,
                        PartnerId = y.PartnerId,
                        Limit = y.Limit,
                        CreateDate = y.CreateDate.ToString("dd.MM.yyyy hh:mm:ss"),
                        EndDate = y.EndDate.ToString("dd.MM.yyyy hh:mm:ss"),
                        CancelDate = y.CancelDate?.ToString("dd.MM.yyyy hh:mm:ss"),
                    }).ToList()
            });

            return Ok(response);
        }

        [HttpGet("{id}/limits/{limitId}")]
        public async Task<ActionResult<PartnerPromoCodeLimit>> GetPartnerLimitAsync(Guid id, Guid limitId)
        {
            try
            {
                var partner = await _partnersService.GetByIdAsync(id);

                var limit = partner.PartnerLimits
                    .FirstOrDefault(x => x.Id == limitId);

                var response = new PartnerPromoCodeLimitResponse()
                {
                    Id = limit.Id,
                    PartnerId = limit.PartnerId,
                    Limit = limit.Limit,
                    CreateDate = limit.CreateDate.ToString("dd.MM.yyyy hh:mm:ss"),
                    EndDate = limit.EndDate.ToString("dd.MM.yyyy hh:mm:ss"),
                    CancelDate = limit.CancelDate?.ToString("dd.MM.yyyy hh:mm:ss"),
                };

                return Ok(response);
            }
            catch (EntityNotFoundException)
            {
                return NotFound();
            }
        }

        [HttpPost("{id}/limits")]
        public async Task<IActionResult> SetPartnerPromoCodeLimitAsync(Guid id, SetPartnerPromoCodeLimitRequest request)
        {
            try
            {
                var newLimit = new PartnerPromoCodeLimit()
                {
                    Limit = request.Limit,
                    EndDate = request.EndDate
                };

                var resultLimit = await _partnersService.SetNewPromoCodeLimitAsync(id, newLimit);

                return CreatedAtAction(nameof(GetPartnerLimitAsync), new {id, limitId = resultLimit.Id}, null);
            }
            catch (PartnerNotActiveException)
            {
                return BadRequest("Данный партнер не активен");
            }
            catch (InvalidLimitException)
            {
                return BadRequest("Лимит должен быть больше 0");
            }
            catch (EntityNotFoundException)
            {
                return NotFound();
            }
        }

        [HttpPost("{id}/canceledLimits")]
        public async Task<IActionResult> CancelPartnerPromoCodeLimitAsync(Guid id)
        {
            try
            {
                await _partnersService.CancelActivePromoCodeLimitAsync(id);
                return NoContent();
            }
            catch (PartnerNotActiveException)
            {
                return BadRequest("Данный партнер не активен");
            }
            catch (EntityNotFoundException)
            {
                return NotFound();
            }
        }
    }
}